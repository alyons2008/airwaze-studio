# Things to Remember

* Always have a default constructor on your models.
* Use `Ctrl + Shift + R` to run your tests.  If your cursor is in a test, it will run specifically that test.  If your cursor is outside of a test, it will run all of the tests.
* When using `@RequestParam`, an error will be thrown if the parameter is not included.  Be sure to use [Optional](https://docs.oracle.com/javase/8/docs/api/java/util/Optional.html) or `@RequestParam(required=false)`;
* Spring automatically imports the `import.sql` file upon startup.  *Be sure to change `import.sql` file to local paths to the CSVs.

# Error Messages
* `ERROR: type "geometry" does not exist`   
Be sure that you have installed all of the geospatial extensions to Postgres

* `Expected 200.  Got 404`  
Check that your path matches the route exactly.  Sometimes a trailing `/` is needed.

* `org.thymeleaf.exceptions.TemplateInputException: Error resolving template "route", template might not exist or might not be accessible ...`  
Check and make sure that you have `@ResponseBody` on your method.  If `@ResponseBody` is not on your method it will attempt to redirect to a template.

* `Failed to replace DataSource with an embedded database for tests.`  
If you want an embedded database please put a supported one on the classpath or use the replace attribute of `@AutoconfigureTestDatabase.`


# Mike's Favorite Intellij Shortcuts
* `Cmd + Shift + v` paste history.  Repaste anything you've pasted in the past.

#Libraries
* [Hibernate Spatial](http://www.baeldung.com/hibernate-spatial)
* [Example SpringMVC Project](https://github.com/LaunchCodeEducation/cheese-mvc/)
* [JSON Path evaluator](http://jsonpath.com/)
* [Pluralsight Example for JSON path](https://www.pluralsight.com/blog/tutorials/introduction-to-jsonpath)

#Helpful Links
* [Every Mac KeyboardShortcut](https://www.danrodney.com/mac/)
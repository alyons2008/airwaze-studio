package com.launchcode.gisdevops.models;

import com.vividsolutions.jts.geom.Geometry;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Airport {


    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer airportId;
    private Geometry airportLatLong;
    private String faaCode;
    private String name;
    private String city;
    private String country;
    private String icao;
    private Integer altitude;
    private String timeZone;

    public Airport() {
    }

    public Airport(Geometry airportLatLong, Integer airportId, String faaCode, String name, String city, String icao, Integer altitude, String timeZone, String country) {
        this.airportLatLong = airportLatLong;
        this.faaCode = faaCode;
        this.name = name;
        this.city = city;
        this.icao = icao;
        this.altitude = altitude;
        this.timeZone = timeZone;
        this.country = country;
        this.airportId = airportId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFaaCode() {
        return faaCode;
    }

    public void setFaaCode(String faaCode) {
        this.faaCode = faaCode;
    }

    public Geometry getAirportLatLong() {
        return airportLatLong;
    }

    public void setAirportLatLong(Geometry airportLatLong) {
        this.airportLatLong = airportLatLong;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public Integer getAltitude() {
        return altitude;
    }

    public void setAltitude(Integer altitude) {
        this.altitude = altitude;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getAirportId() {
        return airportId;
    }

    public void setAirportId(Integer airportId) {
        this.airportId = airportId;
    }
}
